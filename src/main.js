import Vue from 'vue';
import Variable from './components/variable';

import Variables from './variables'

Vue.component(Variable.name, Variable);

import Docute from 'docute';

new Docute({
  target: '#app',
  title: 'JuniperBot - Documentation',
  sourcePath: process.env.DOCS_ROOT,
  editLinkBase: 'https://gitlab.com/JuniperBot/documentation/blob/master/static',
  editLinkText: 'Edit this page on GitLab',
  componentMixins: [
    {
      data() {
        return {
          vars: Variables
        }
      }
    }
  ],
  nav: [
    {
      title: 'Home',
      link: '/'
    },
    {
      title: 'FAQ',
      link: '/faq'
    },
    {
      title: 'JuniperBot',
      link: 'https://juniperbot.ru'
    }
  ],
  sidebar: [
    {
      title: 'Main',
      links: [
        {
          title: 'Intro',
          link: '/'
        }
      ]
    },
    {
      title: 'Commands',
      links: [
        {
          title: 'Commands and behaviour',
          link: '/commands/common'
        },
        {
          title: 'Information',
          link: '/commands/info'
        },
        {
          title: 'Ranking',
          link: '/commands/ranking'
        },
        {
          title: 'Music',
          link: '/commands/music'
        },
        {
          title: 'Moderation',
          link: '/commands/moderation'
        },
        {
          title: 'Utility',
          link: '/commands/utility'
        },
        {
          title: 'Forest Fuss',
          link: '/commands/forestfuss'
        },
        {
          title: 'Fun',
          link: '/commands/fun'
        }
      ]
    },
    {
      title: 'Misc',
      links: [
        {
          title: 'FAQ',
          link: '/faq'
        },
        {
          title: 'Variables',
          link: '/variables'
        },
        {
          title: 'Radio Stations',
          link: '/radio'
        },
        {
          title: 'Docs tips',
          link: '/snippets'
        }
      ]
    }
  ],
  overrides: {
    '/': {
      language: 'English'
    },
    '/ru/': {
      language: 'Русский',
      title: 'JuniperBot - Документация',
      editLinkText: 'Редактировать эту страницу в GitLab',
      nav: [
        {
          title: 'Главная',
          link: '/ru/'
        },
        {
          title: 'FAQ',
          link: '/ru/faq'
        },
        {
          title: 'JuniperBot',
          link: 'https://juniperbot.ru'
        }
      ],
      sidebar: [
        {
          title: 'Основное',
          links: [
            {
              title: 'Введение',
              link: '/ru/'
            }
          ]
        },
        {
          title: 'Команды',
          links: [
            {
              title: 'Команды и поведение',
              link: '/ru/commands/common'
            },
            {
              title: 'Информация',
              link: '/ru/commands/info'
            },
            {
              title: 'Рейтинг',
              link: '/ru/commands/ranking'
            },
            {
              title: 'Музыка',
              link: '/ru/commands/music'
            },
            {
              title: 'Модерирование',
              link: '/ru/commands/moderation'
            },
            {
              title: 'Утилиты',
              link: '/ru/commands/utility'
            },
            {
              title: 'Лесная Возня',
              link: '/ru/commands/forestfuss'
            },
            {
              title: 'Весёлое',
              link: '/ru/commands/fun'
            }
          ]
        },
        {
          title: 'Прочее',
          links: [
            {
              title: 'FAQ',
              link: '/ru/faq'
            },
            {
              title: 'Переменные',
              link: '/ru/variables'
            },
            {
              title: 'Радиостанции',
              link: '/ru/radio'
            },
            {
              title: 'Подсказки по документации',
              link: '/ru/snippets'
            }
          ]
        }
      ]
    }
  }
});
