'use strict';
module.exports = {
  NODE_ENV: '"production"',
  DOCS_ROOT: '"https://raw.githubusercontent.com/JuniperBot/Documentation/master/static/"'
};
