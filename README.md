# JuniperBot Documentation

To run this locally:

1. [Download](https://nodejs.org/dist/v10.14.1/node-v10.14.1-x64.msi) and install NodeJS v10.14.1;
2. Enter repository root in command line;
3. Run following:
```bash
npm i
npm start
```
Now you can open provided link in browser to see and edit documentation locally.
