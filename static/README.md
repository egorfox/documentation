# Introduction

## Invite

To invite the bot to your server you should do following:

1. Open <https://juniperbot.ru/>;
2. Click big orange button `Add to Discord` and follow its instructions.

or:

1. Open <https://juniperbot.ru/>;
2. Click `Log In` in top right corner and authorize with your Discord account;
3. Click on your avatar or username and `My Servers` button in menu;
4. Locate your server and click `Invite`;
5. Follow the Discord instructions.

<p class="warning">
  Please keep all rights checkboxes selected to let JuniperBot operate as intended.
</p>
<p class="tip">
  Interface and commands language by default depends in server's region. For example, if region is Russia, then it would be russian.<br/>
  Language can be configured in server's dashboard in <b>Common</b> section.
</p>

## Configure

To configure your bot you should do following:
1. Open <https://juniperbot.ru/>;
2. Click `Log In` in top right corner and authorize with your Discord account;
3. Click on your avatar or username and `My Servers` button in menu;
4. Locate your server and click on it;
5. Now you're in your server's dashboard and configure the bot as you wish;
6. You're awesome!
