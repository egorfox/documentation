
# Supported radio stations

You can listen some popular radio stations using [play](/commands/music#play) command with link to stream you want to listen. You can see full list of supported streams from tables below. For example: `!play http://radio-srv1.11one.ru/record192k.mp3`

If you want to see your favourite radio station here, you can ask for it at our <jb-variable type="link" name="supportServer" linkName="support server"/>

<p class="tip">
  You should allow stream audio on your server's dashboard in <b>Music</b> section.
</p>

<p class="warning">
  Stable availability of these radio stations is not guaranteed and depends only on the radio stations themselves.
</p>

### Radio Record

**Official website:** http://www.radiorecord.fm/

| Name | Stream link |
|---|---|
| Main 1 | http://radio-srv1.11one.ru/record192k.mp3 |
| Main 2 | http://www.radiorecord.fm/listen/record_192k |
| Main 3 | http://air.radiorecord.ru:8101/rr_320 |
| Superdisco 90s | http://air.radiorecord.ru:8102/sd90_320 |
| Trancemission | http://air.radiorecord.ru:8102/tm_320 |
| Russian Mix | http://air.radiorecord.ru:8102/rus_320 |
| Mdl FM | http://air.radiorecord.ru:8102/mdl_320 |
| Gop FM | http://air.radiorecord.ru:8102/gop_320 |
| Vip Mix | http://air.radiorecord.ru:8102/vip_320 |
| Pirate Station | http://air.radiorecord.ru:8102/ps_320 |
| Yo! FM | http://air.radiorecord.ru:8102/yo_320 |
| Pump'n'Klubb | http://air.radiorecord.ru:8102/pump_320 |
| Teodor Hardstyle | http://air.radiorecord.ru:8102/teo_320 |
| Record Chill-Out | http://air.radiorecord.ru:8102/chil_320 |
| Record Club | http://air.radiorecord.ru:8102/club_320 |
| Record Deep | http://air.radiorecord.ru:8102/deep_320 |
| Record Breaks | http://air.radiorecord.ru:8102/brks_320 |
| Record Dancecore | http://air.radiorecord.ru:8102/dc_320 |
| Record Dubstep | http://air.radiorecord.ru:8102/dub_320 |
| Record Trap | http://air.radiorecord.ru:8102/trap_320 |
| Record Techno | http://air.radiorecord.ru:8102/techno_320 |
| Minimal Techno | http://air.radiorecord.ru:8102/mini_320 |
| Future House | http://air.radiorecord.ru:8102/fut_320 |
| Rock Radio | http://air.radiorecord.ru:8102/rock_320 |

### Nightwave Plaza

**Official website:** https://plaza.one

**Stream link:** http://radio.plaza.one/mp3

### ChipBit

**Official website:** http://chipbit.net/

**Stream link:** http://stream.chipbit.net:8000/live

### Wargaming.fm

**Official website:** http://wargaming.fm/

| Name | Stream link |
|---|---|
| WGFM Main channel | http://sv.wargaming.fm:8051/128 |
| WGFM Secondary channel | http://sv.wargaming.fm:8052/128 |
| WGFM Trance | http://sv.wargaming.fm:8053/128 |
| WGFM Rock | http://sv.wargaming.fm:8054/12 |

### Xiт FM

**Official website:** https://www.hitfm.ua/

| Name | Stream link |
|---|---|
| Хіт FM | http://online-hitfm.tavrmedia.ua/HitFM |
| Хіт FM Українські хіти | http://online-hitfm2.tavrmedia.ua/HitFM_Ukr |
| Хіт FM Найбільші хіти | http://online-hitfm2.tavrmedia.ua/HitFM_Best |
| Хіт FM Сучасні хіти | http://online-hitfm2.tavrmedia.ua/HitFM_Top |

### NRJ Ukraine

**Official website:** http://nrj.ua/

| Name | Stream link |
|---|---|
| NRJ | http://cast.nrj.in.ua/nrj |
| NRJ Hot 40 | http://cast2.nrj.in.ua/nrj_hot |
| NRJ All Hits | http://cast2.nrj.in.ua/nrj_hits |
| NRJ Party Hits | http://cast2.nrj.in.ua/nrj_party |

### Radiogroup «Ukraine Media Holding»

**Official website:** http://cast.radiogroup.com.ua/

**Stream link:** all available mount points at http://cast.radiogroup.com.ua/

### Kiss FM

**Official website:** https://www.kissfm.ua/

| Name | Stream link |
|---|---|
| Ефір KISS FM | http://online-kissfm.tavrmedia.ua/KissFM |
| KISS FM Ukrainian | http://online-kissfm2.tavrmedia.ua/KissFM_Ukr |
| KISS FM Deep | http://online-kissfm2.tavrmedia.ua/KissFM_Deep |
| KISS FM Digital | http://online-kissfm2.tavrmedia.ua/KissFM_Digital |

### Radio ROKS

**Official website:** https://www.radioroks.ua/

| Name | Stream link |
|---|---|
| Ефір Radio ROKS | http://online-radioroks2.tavrmedia.ua/RadioROKS |
| Український рок | http://online-radioroks2.tavrmedia.ua/RadioROKS_Ukr |
| Новий Рок | http://online-radioroks2.tavrmedia.ua/RadioROKS_NewRock |
| Hard'n'Heavy | http://online-radioroks2.tavrmedia.ua/RadioROKS_HardnHeavy |
| Рок-Балади | http://online-radioroks2.tavrmedia.ua/RadioROKS_Ballads |

### Europa Plus

**Official website:** http://www.europaplus.ru/

| Name | Stream link |
|---|---|
| Main | http://ep128.hostingradio.ru:8030/ep128 |
| Top 40 | http://eptop128server.streamr.ru:8033/eptop128 |
| R&B | http://eprnb128server.streamr.ru:8061/eprnb128 |

### POWER FM

**Stream link:** http://radio.powerdance.ru:8018/power

### Radio Hermitage

**Official website:** http://www.rhfm.ru/radio.php

**Stream link:** http://91.190.127.185:8000/live_test

### Trigger

**Official website:** http://www.trigger.fm

**Stream link:** http://main.trigger.fm/stream/mainmp3
