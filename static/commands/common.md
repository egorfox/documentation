# Commands and behaviour

## Prefix
JuniperBot uses prefix `!` as default. This documentation will describe commands using this prefix too but it can be different in case if you have been set your own prefix in your server's dashboard in **Common** section.

<p class="tip">
JuniperBot supports mentions as prefix. For example: <code>@JuniperBot#6999 help</code> is the same as <code>!help</code>
</p>

## Aliases
JuniperBot supports different languages for interface (replies) and commands. You can configure them at your server's dashboard in **Common** section.

This documentation will describe commands in English but also each command description will have table with command aliases for each supported language.

## !help
This command is your best assistant. It will return the list of commands available for this member.

#### Aliases
|English|Russian|
|---|---|
|!help|!хелп|

#### Usage
```
!хелп
```
#### Result
Bot will return list of commands available for this member.

<p class="warning">
The result of this command is list of command <b>available for this member</b> excluding those, to which member doesn't has access for some reasons. <br/>
For example, regular member will not see moderation commands or commands available only for specific set of roles.
</p>

---

As you can see all commands are grouped (fun, information, music and so on). To get more detailed info about commands of specific group, just type group name with `!help` command.

#### Usage for group
```
!help music
```
#### Result for group
Bot will return list of music commands with short descriptions for each of them.
