# Information commands


## !info
Returns some useful information about bot:

* Prefix;
* Developer name;
* Version and build date;
* Useful links;
* Platform information.

#### Aliases
|English|Russian|
|---|---|
|!info|!инфо|

#### Usage
```
!info
```
#### Result
Bot will return some useful info about itself.


## !stats
Returns bot some statistics:

* Servers count;
* Users and channels count;
* Platform uptime;
* Commands usage statistics.

#### Aliases
|English|Russian|
|---|---|
|!stats|!стат|

#### Usage
```
!stats
```
#### Result
Bot will return some statistics.


## !serverinfo
Returns useful information about current server:

* Member count grouped by statuses;
* Channel count grouped by type;
* Verification level;
* Region;
* Owner;
* Creation date.

#### Aliases
|English|Russian|
|---|---|
|!serverinfo|!сервер|

#### Usage
```
!serverinfo
```
#### Result
Bot will return useful information about current server.


## !user
Returns information about current or `@mentioned` member:

* Member's own information specified by [!bio](/commands/info#bio) command;
* Member's name;
* Online status;
* Server joining date;
* Registration date;
* All information from [!rank](/commands/ranking#rank) command for this member if ranking is enabled for this server.

#### Aliases
|English|Russian|
|---|---|
|!user|!юзер|

#### Usage
```
!user
!user @member
```
#### Result
Bot will return information about current or `@mentioned` member:


## !bio
Use this command to show or set up your own information about yourself, which will be also available by [!user](/commands/info#user) command.

#### Aliases
|English|Russian|
|---|---|
|!bio|!осебе|

#### Usage
```
!bio I like the cookies!
```
#### Result
Bot will accept your information by ✅ reaction in case of success.

#### Usage
```
!bio
```
#### Result
Bot will return current information with short help.

<p class="tip">
  If you want to clear information, enter <code>!bio -</code>
</p>
