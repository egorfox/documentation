# Переменные шаблонов сообщений
Вы можете использовать нижеперечисленные переменные в любых шаблонах сообщений, которые можете найти в панели управления своим сервером.

Их можно использовать, написав внутри:
 * Тела сообщения; 
 * Автора панели;
 * Наименования панели;
 * Футера (подвала) панели;
 * Полях панели (в названии и значении).

## Глобальные переменные {server}
Это глобальные переменные, возвращающие некоторую полезную информацию о Вашем сервере.

|Переменная|Тип|Описание|
|---|---|---|
|`{server}`|Строка|Возвращает название сервера (сокращение от `{server.name}`.|
|`{server.id}`|Число|Возвращает уникальный идентификатор сервера.|
|`{server.name}`|Строка|Возвращает название сервера.|
|`{server.region}`|Строка|Возвращает название региона сервера с иконкой-эмоцией.|
|`{server.afkTimeout}`|Число|Возвращает AFK таймаут голосового канала в минутах.|
|`{server.afkChannel}`|Строка|Возвращает название AFK голосового канала.|
|`{server.memberCount}`|Число|Возвращает количество участников на сервере.|
|`{server.createdAt}`|[ДатаВремя](/ru/variables#кастомизация-переменных-датавремя)|Возвращает дату и время создания сервера.|

## Глобальные переменные {member}
Это глобальные переменные, возвращающие некоторую полезную информацию об участнике, который вызвал соответствующую команду или событие (поднятие уровня в рейтинге, например).

|Переменная|Тип|Описание|
|---|---|---|
|`{member}`|Упоминание|Возвращает упоминание участника (сокращение от `{member.mention}`).|
|`{member.id}`|Число|Возвращает уникальный идентификатор участника.|
|`{member.mention}`|Упоминание|Возвращает упоминание участника.|
|`{member.nickname}`|Строка|Возвращает никнейм участника или его глобальное имя, если никнейм не установлен.|
|`{member.name}`|Строка|Возврашает глобальное имя участника (без дискриминатора).|
|`{member.discriminator}`|Строка|Возвращает дискриминатор участника (тег, 4 числа после # имени учетной записи).|
|`{member.joinedAt}`|[ДатаВремя](/ru/variables#кастомизация-переменных-датавремя)|Возвращает дату и время присоединения участника на сервер.|
|`{member.createdAt}`|[ДатаВремя](/ru/variables#кастомизация-переменных-датавремя)|Возвращает дату и время создания учетной записи участника.|
|`{member.status}`|Строка|Возвращает статус (В сети, Нет на месте, Не беспокоить, Оффлайн) участника с иконкой-эмоцией.|
|`{member.avatarUrl}`|Строка|Возвращает ссылку на аватар участника.|

## Глобальные переменные {channel}
Это глобальные переменные, возвращающие некоторую полезную информацию о целевом канале отправляемого сообщения (исключая сообщения в личку).

|Переменная|Тип|Описание|
|---|---|---|
|`{channel}`|Упоминание|Возвращает упоминание канала (сокращение от `{channel.mention}`).|
|`{channel.id}`|Число|Возвращает уникальный идентификатор канала.|
|`{channel.name}`|Строка|Возвращает название канала.|
|`{channel.mention}`|Упоминание|Возвращает упоминание канала.|
|`{channel.topic}`|Строка|Возвращает описание (топик) канала.|
|`{channel.position}`|Число|Возвращает номер позиции канала в списке (1 - верх списка).|
|`{channel.createdAt}`|[ДатаВремя](/ru/variables#кастомизация-переменных-датавремя)|Возвращает дату и время создания учетной записи канала.|

## Переменные рейтинга
Это дополнительные переменные для сообщения повышения уровня рейтинга.

Список включает в себя глобальные переменные [`{server}`](/variables#global-server-variables-set), [`{member}`](/variables#global-member-variables-set) и [`{channel}`](/variables#global-channel-variables-set).

|Переменная|Тип|Описание|
|---|---|---|
|`{level}`|Число|Новый уровень участника.|

## Переменные польз. команд
Это дополнительные переменные для пользовательских команд.

Список включает в себя глобальные переменные [`{server}`](/variables#global-server-variables-set), [`{member}`](/variables#global-member-variables-set) и [`{channel}`](/variables#global-channel-variables-set).

|Переменная|Тип|Описание|
|---|---|---|
|`{content}`|Строка|Текст, следуемый за введенной командой. Например, для команды `!команда блаблабла` контентом будет `блаблабла`.|

## Кастомизация переменных ДатаВремя
Любые команды типа **ДатаВремя** могут быть кастомизированы под разные форматы дат и времени.

<p class="tip">
Используйте следующую таблицу в качестве примера, поскольку необходимо заменить <code>datetime</code> с реальным названием переменной. Например, вы можете использовать <code>{server.createdAt.mediumDate}</code> для получения только даты создания сервера.
</p>

|Переменная|Описание|
|---|---|
|`{datetime}`|Возвращает дату и время в обычном формате (сокращение от `{datetime.mediumDateTime}`), например:<br />`7 мая 2017 г., 22:48:55`|
|`{datetime.shortTime}`|Возвращает время в кратком формате, например:<br />`22:48`|
|`{datetime.mediumTime}`|Возвращает время в обычном формате, например:<br />`22:48:55`|
|`{datetime.longTime}`|Возвращает время в длинном формате, например:<br />`22:48:55 SAMT`|
|`{datetime.fullTime}`|Возвращает время в полном формате, например:<br />`22:48:55 Самарское стандартное время`|
|`{datetime.shortDate}`|Возвращает дату в кратком формате, например:<br />`07.05.17`|
|`{datetime.mediumDate}`|Возвращает дату в обычном формате, например:<br />`7 мая 2017 г.`|
|`{datetime.fullDate}`|Возвращает дату в полном формате, например:<br />`воскресенье, 7 мая 2017 г.`|
|`{datetime.shortDateTime}`|Возвращает дату и время в кратком формате, например:<br />`07.05.17, 22:48`|
|`{datetime.mediumDateTime}`|Возвращает дату и время в обычном формате, например:<br />`7 мая 2017 г., 22:48:55`|
|`{datetime.longDateTime}`|Возвращает дату и время в длинном формате, например:<br />`7 мая 2017 г., 22:48:55 SAMT`|
|`{datetime.fullDateTime}`|Возвращает дату и время в полном формате, например:<br />`воскресенье, 7 мая 2017 г., 22:48:55 Самарское стандартное время`|
