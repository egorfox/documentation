# Список доступных радиостанций

Вы можете прослушивать некоторые популярные радиостанции используя привычную [команду воспроизведения](/ru/commands/music#плей), указывая ссылку на интересуемый вас поток из таблиц ниже. Например:
 `!плей http://radio-srv1.11one.ru/record192k.mp3`

Для добавления новой поддерживаемой радиостанции в указанные списки, вы можете обратиться на <jb-variable type="link" name="supportServer" linkName="сервер поддержки"/>
<p class="tip">
  Возможно, вам потребуется разрешить потоковое аудио в панели управления сервером в разделе <b>Музыка</b>.
</p>

<p class="warning">
  Полная работоспособность радиостанций ниже не гарантируется и полностью зависит от самих радиостанций.
</p>

### Radio Record

**Официальный сайт:** http://www.radiorecord.fm/

| Название | Ссылка на поток |
|---|---|
| Основное 1 | http://radio-srv1.11one.ru/record192k.mp3 |
| Основное 2 | http://www.radiorecord.fm/listen/record_192k |
| Основное 3 | http://air.radiorecord.ru:8101/rr_320 |
| Супердискотека 90-х | http://air.radiorecord.ru:8102/sd90_320 |
| Trancemission | http://air.radiorecord.ru:8102/tm_320 |
| Russian Mix | http://air.radiorecord.ru:8102/rus_320 |
| Медляк FM | http://air.radiorecord.ru:8102/mdl_320 |
| Гоп FM | http://air.radiorecord.ru:8102/gop_320 |
| Vip Mix | http://air.radiorecord.ru:8102/vip_320 |
| Pirate Station | http://air.radiorecord.ru:8102/ps_320 |
| Yo! FM | http://air.radiorecord.ru:8102/yo_320 |
| Pump'n'Klubb | http://air.radiorecord.ru:8102/pump_320 |
| Teodor Hardstyle | http://air.radiorecord.ru:8102/teo_320 |
| Record Chill-Out | http://air.radiorecord.ru:8102/chil_320 |
| Record Club | http://air.radiorecord.ru:8102/club_320 |
| Record Deep | http://air.radiorecord.ru:8102/deep_320 |
| Record Breaks | http://air.radiorecord.ru:8102/brks_320 |
| Record Dancecore | http://air.radiorecord.ru:8102/dc_320 |
| Record Dubstep | http://air.radiorecord.ru:8102/dub_320 |
| Record Trap | http://air.radiorecord.ru:8102/trap_320 |
| Record Techno | http://air.radiorecord.ru:8102/techno_320 |
| Minimal Techno | http://air.radiorecord.ru:8102/mini_320 |
| Future House | http://air.radiorecord.ru:8102/fut_320 |
| Rock Radio | http://air.radiorecord.ru:8102/rock_320 |

### Nightwave Plaza

**Официальный сайт:** https://plaza.one

**Ссылка на поток:** http://radio.plaza.one/mp3

### ChipBit

**Официальный сайт:** http://chipbit.net/

**Ссылка на поток:** http://stream.chipbit.net:8000/live

### Wargaming.fm

**Официальный сайт:** http://wargaming.fm/

| Название | Ссылка на поток |
|---|---|
| WGFM Главный канал | http://sv.wargaming.fm:8051/128 |
| WGFM Второй канал | http://sv.wargaming.fm:8052/128 |
| WGFM Trance | http://sv.wargaming.fm:8053/128 |
| WGFM Rock | http://sv.wargaming.fm:8054/12 |

### Xiт FM

**Официальный сайт:** https://www.hitfm.ua/

| Название | Ссылка на поток |
|---|---|
| Хіт FM | http://online-hitfm.tavrmedia.ua/HitFM |
| Хіт FM Українські хіти | http://online-hitfm2.tavrmedia.ua/HitFM_Ukr |
| Хіт FM Найбільші хіти | http://online-hitfm2.tavrmedia.ua/HitFM_Best |
| Хіт FM Сучасні хіти | http://online-hitfm2.tavrmedia.ua/HitFM_Top |

### NRJ Украина

**Официальный сайт:** http://nrj.ua/

| Название | Ссылка на поток |
|---|---|
| NRJ | http://cast.nrj.in.ua/nrj |
| NRJ Hot 40 | http://cast2.nrj.in.ua/nrj_hot |
| NRJ All Hits | http://cast2.nrj.in.ua/nrj_hits |
| NRJ Party Hits | http://cast2.nrj.in.ua/nrj_party |

### Радиогруппа «Украинский Медиа Холдинг»

**Официальный сайт:** http://cast.radiogroup.com.ua/

**Ссылки на поток:** все доступные на http://cast.radiogroup.com.ua/

### Kiss FM

**Официальный сайт:** https://www.kissfm.ua/

| Название | Ссылка на поток |
|---|---|
| Ефір KISS FM | http://online-kissfm.tavrmedia.ua/KissFM |
| KISS FM Ukrainian | http://online-kissfm2.tavrmedia.ua/KissFM_Ukr |
| KISS FM Deep | http://online-kissfm2.tavrmedia.ua/KissFM_Deep |
| KISS FM Digital | http://online-kissfm2.tavrmedia.ua/KissFM_Digital |

### Radio ROKS

**Официальный сайт:** https://www.radioroks.ua/

| Название | Ссылка на поток |
|---|---|
| Ефір Radio ROKS | http://online-radioroks2.tavrmedia.ua/RadioROKS |
| Український рок | http://online-radioroks2.tavrmedia.ua/RadioROKS_Ukr |
| Новий Рок | http://online-radioroks2.tavrmedia.ua/RadioROKS_NewRock |
| Hard'n'Heavy | http://online-radioroks2.tavrmedia.ua/RadioROKS_HardnHeavy |
| Рок-Балади | http://online-radioroks2.tavrmedia.ua/RadioROKS_Ballads |

### Европа-Плюс

**Официальный сайт:** http://www.europaplus.ru/

| Название | Ссылка на поток |
|---|---|
| Основное | http://ep128.hostingradio.ru:8030/ep128 |
| Топ 40 | http://eptop128server.streamr.ru:8033/eptop128 |
| R&B | http://eprnb128server.streamr.ru:8061/eprnb128 |

### POWER FM

**Ссылка на поток:** http://radio.powerdance.ru:8018/power

### Радио Эрмитаж

**Официальный сайт:** http://www.rhfm.ru/radio.php

**Ссылка на поток:** http://91.190.127.185:8000/live_test

### Trigger

**Официальный сайт:** http://www.trigger.fm

**Ссылка на поток:** http://main.trigger.fm/stream/mainmp3
