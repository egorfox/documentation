<p class="warning">
  Документация в процессе написания. Пожалуйста, будьте терпеливы.
</p>

# Утилитарные команды

## !напомни
Тут должен быть текст.

#### Псевдонимы
|English|Russian|
|---|---|
|!remind|!напомни|

#### Пример использования
```
!напомни через 2 недели 5 дней фыр!
```
#### Результат выполнения
Тут должен быть текст.


## !викифур
Тут должен быть текст.

#### Псевдонимы
|English|Russian|
|---|---|
|!wikifur|!викифур|

#### Пример использования
```
!викифур название
```
#### Результат выполнения
Тут должен быть текст.


## !steam
Тут должен быть текст.

#### Псевдонимы
|English|Russian|
|---|---|
|!steam |!steam|

#### Пример использования
```
!steam game
```
#### Результат выполнения
Тут должен быть текст.


## !t
Тут должен быть текст.

#### Псевдонимы
|English|Russian|
|---|---|
|!t|!t|

#### Пример использования
```
!t example
```
#### Результат выполнения
Тут должен быть текст.


## !ранд
Тут должен быть текст.

#### Псевдонимы
|English|Russian|
|---|---|
|!rand |!ранд|

#### Пример использования
```
!ранд 100 200
```
#### Результат выполнения
Тут должен быть текст.


## !аватар
Тут должен быть текст.

#### Псевдонимы
|English|Russian|
|---|---|
|!avatar|!аватар|

#### Пример использования
```
!аватар @пользователь#1234
```
#### Результат выполнения
Тут должен быть текст.