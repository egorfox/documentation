# Введение

## Добавление бота

Чтобы добавить бота на сервер, нужно:

1. Зайти на <https://juniperbot.ru/>;
2. Кликнуть большую кнопку `Добавить в Discord` и следовать инструкциям.

или:

1. Зайти на <https://juniperbot.ru/>;
2. Кликнуть кнопку `Войти` в верхнем правом углу и войти в свой аккаунт Discord;
3. Кликнуть на аватарку или имя пользователя и в открывшемся меню кнопку `Мои серверы`;
4. Найти нужный сервер и кликнуть `Пригласить`;
5. Следовать инструкции Discord.

<p class="warning">
  Для корректного функционирования JuniperBot рекомендуется оставить все галочки при выборе прав.
</p>
<p class="tip">
  Язык интерфейса и команд по-умолчанию зависит от региона сервера. Если регион сервера — Россия, то интерфейс и команды будут на русском.<br/>
  Язык можно поменять в панели управления сервером в разделе <b>Общие</b>.
</p>

## Настройка бота

Чтобы настроить бота, нужно:
1. Зайти на <https://juniperbot.ru/>;
2. Кликнуть кнопку `Войти` в верхнем правом углу и войти в свой аккаунт Discord если ещё не сделали это;
3. Кликнуть на свою аватарку или имя пользователя и в открывшемся меню кнопку `Мои серверы`;
4. Найти нужный сервер и кликнуть по нему;
5. Вы вошли в панель управления данным сервером и можете настроить здесь все как вам угодно;
6. Вы прекрасны!
