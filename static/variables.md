# Message Template Variables
You can use following variables in any message templates you can find in your server's dashboard.

These can be used by typing them inside:
 * Message content; 
 * Embed's author;
 * Embed's title;
 * Embed's footer;
 * Embed's fields (both title and value).

## Global {server} variables set
This is global variables set returning some useful info about your server.

|Variable|Type|Description|
|---|---|---|
|`{server}`|String|Gives the server name (short-alias of `{server.name}`.|
|`{server.id}`|Number|Gives the server unique identifier.|
|`{server.name}`|String|Gives the server name.|
|`{server.region}`|String|Gives the server region name with emoji icon.|
|`{server.afkTimeout}`|Number|Gives the AFK Timeout of the voice channel in minutes.|
|`{server.afkChannel}`|String|Gives the name of AFK voice channel.|
|`{server.memberCount}`|Number|Gives the amount of members in your server.|
|`{server.createdAt}`|[DateTime](/variables#datetime-variables-customization)|Gives the date and time the server got created.|

## Global {member} variables set
This is global variable set returning some useful info about member invoked related command or event (ranking level up, etc).

|Variable|Type|Description|
|---|---|---|
|`{member}`|Mention|Gives the mention for member (short-alias of `{member.mention}`).|
|`{member.id}`|Number|Gives the member unique identifier.|
|`{member.mention}`|Mention|Gives the mention for member.|
|`{member.nickname}`|String|Gives the nickname of the member or the username if no nickname was set.|
|`{member.name}`|String|Gives the global member name (without discriminator).|
|`{member.discriminator}`|String|Gives the member discriminator (tag, 4 numbers after the # of username).|
|`{member.joinedAt}`|[DateTime](/variables#datetime-variables-customization)|Gives the date and time when the member was joined the server.|
|`{member.createdAt}`|[DateTime](/variables#datetime-variables-customization)|Gives date and time when the member account was created.|
|`{member.status}`|String|Gives the status (online, afk, dnd, offline) of the member with emoji icon.|
|`{member.avatarUrl}`|String|Gives the link to the member avatar.|

## Global {channel} variables set
This is global variable set returning some useful info about target channel of related message (excluding DM's).

|Variable|Type|Description|
|---|---|---|
|`{channel}`|Mention|Gives the mention for channel (short-alias of `{channel.mention}`).|
|`{channel.id}`|Number|Gives the channel unique identifier.|
|`{channel.name}`|String|Gives the channel name.|
|`{channel.mention}`|Mention|Gives the mention for channel.|
|`{channel.topic}`|String|Gives the channel topic.|
|`{channel.position}`|Number|Gives the position of the channel in the list (1 is the top of the list).|
|`{channel.createdAt}`|[DateTime](/variables#datetime-variables-customization)||Gives the date and time the channel got created.|

## Ranking variables set
This is additional ranking variables set for an level-up template.

It includes global [`{server}`](/variables#global-server-variables-set), [`{member}`](/variables#global-member-variables-set) and [`{channel}`](/variables#global-channel-variables-set) variables sets.

|Variable|Type|Description|
|---|---|---|
|`{level}`|Number|New level of member.|

## Custom commands variables set
This is additional custom commands variables set.

It includes global [`{server}`](/variables#global-server-variables-set), [`{member}`](/variables#global-member-variables-set) and [`{channel}`](/variables#global-channel-variables-set) variables sets.

|Variable|Type|Description|
|---|---|---|
|`{content}`|String|Member's content typed with command, e.g. if `!command blablabla` invoked, the content is `blablabla`.|

## DateTime variables customization
Any variables of **DateTime** type can be customized by different date and time formats. 

<p class="tip">
Use following table as reference, you must replace <code>datetime</code> with an actual DateTime variable name, e.g. you can use <code>{server.createdAt.mediumDate}</code> to get only date of your server creation.
</p>

|Variable|Description|
|---|---|
|`{datetime}`|Gives the medium date and time string (snort-alias of `{datetime.mediumDateTime}`), e.g:<br />`May 7, 2017, 10:48:55 PM`|
|`{datetime.shortTime}`|Gives the short time-only string, e.g:<br />`10:48 PM`|
|`{datetime.mediumTime}`|Gives the medium time-only string, e.g:<br />`10:48:55 PM`|
|`{datetime.longTime}`|Gives the long time-only string, e.g:<br />`10:48:55 PM SAMT`|
|`{datetime.fullTime}`|Gives the full time-only string, e.g:<br />`10:48:55 PM Samara Standard Time`|
|`{datetime.shortDate}`|Gives the short date-only string, e.g:<br />`5/7/17`|
|`{datetime.mediumDate}`|Gives the medium date-only string, e.g:<br />`May 7, 2017`|
|`{datetime.fullDate}`|Gives the full date-only string, e.g:<br />`Sunday, May 7, 2017`|
|`{datetime.shortDateTime}`|Gives the short date and time string, e.g:<br />`5/7/17, 10:48 PM`|
|`{datetime.mediumDateTime}`|Gives the medium date and time string, e.g:<br />`May 7, 2017, 10:48:55 PM`|
|`{datetime.longDateTime}`|Gives the long date and time string, e.g:<br />`May 7, 2017 at 10:48:55 PM SAMT`|
|`{datetime.fullDateTime}`|Gives the full date and time string, e.g:<br />`Sunday, May 7, 2017 at 10:48:55 PM Samara Standard Time`|
