# Frequently Asked Questions

## How to add the bot and where I can configure it?
**TL;DR**: https://juniperbot.ru/

Please read [introduction](/) of this documentation.

## Why "Member rating" doesn't shows my roles?
1. Bot doesn't have **Manage roles** permission;
2. Bot's own role (JuniperBot as default) is lower than roles you want to see there.

## What is cookie box in member's rank and profile?
Just counter, karma or reputation system. To increment that counter of any member, you can:  
1. Type message with `@mention` to that member and cookie 🍪 emoji. For example: `@NicePerson#1234 :cookie:`
2. Add 🍪 reaction to message of desired member.

<p class="warning">
  One member can give the 🍪 to another only once within 10 minutes</b>.
</p>

## Can we play bot's playlist again and how to do that?
<jb-variable type="link" name="donateLink" linkName="Patrons"/> can do that using [!play](/commands/music#play) command with playlist link specified.

Playlist links are always available at playback messages.
#### Usage
```
!play https://juniperbot.ru/playlist/99eb328f-d970-4265-ae6f-07c1d7ac8682
```

## Will be any economic system here?
Will not. At all. <b>Never.</b> Use another bots who are originally designed for economic systems.

## Will you support VKontakte playlists and music?
Nope. Say thanks to greedy copywriters who forced [VKontakte](https://vk.com) to close public access to their music.

## When will this and that be?
Sometimes. Maybe. Depends on the free time, desire and motivation of the developer.

## How to calculate exp required for level?
Use this formula: 
```javascript
exp = 5 * (level * level) + (50 * level) + 100
```
