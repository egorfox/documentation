# Documentation tips

## CSS Helpers

CSS helpers make your docs even more readable.

##### p.tip

Show some tips in your doc:

```html
<p class="tip">
  This is for beginners and pros, just enjoy!
</p>
```

And you get:

<p class="tip">
  This is for beginners and pros, just enjoy!
</p>

**Note that you can still use markdown inside the HTML!**

##### p.warning

Similar to `p.tip` but it looks more serious:

```html
<p class="warning">
  Do not do like this, do it that way please. If you still can't help doing such way, we will call your mom and order some pizza to let you know, you're in trouble!
</p>
```

And you get:

<p class="warning">
  Do not do like this, do it that way please. If you still can't help doing such way, we will call you mom and order some pizza to let you know, you're in trouble!
</p>

##### p.danger

```html
<p class="danger">
  This is really dangerouse, watch out!
</p>
```

And you get:

<p class="danger">
  This is really dangerous, watch out!
</p>

---

If you don't like tips with background color, remove it by adding `no-bg` class name:

```html
<p class="warning no-bg">
  How is it going?
</p>
```

And you get:

<p class="warning no-bg">
  How is it going?
</p>

## !command
Executes some useful command

#### Aliases
|English|Russian|
|---|---|
|!command|!команда|

#### Required permissions (if any)
* Moderation rights

#### Usage
```
!command <parameter>
```
#### Result
Bot just did some useful action